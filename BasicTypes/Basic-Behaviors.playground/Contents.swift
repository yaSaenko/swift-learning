import UIKit

struct MyStruct {
    let value: String
}


// MARK: - Equatable
/// `Equatable` is `basic` Protocol for `HUGE AMOUNT` of Protocols
extension MyStruct: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.value == rhs.value
    }
}

let st0 = MyStruct(value: "0")
let st1 = MyStruct(value: "1")
let array = Array(0...5)
    .map { String($0)}
    .map { MyStruct(value: $0)}

//print(st0 == st1)

//print(array.contains { $0 == st0 })

// Match arguments by their values
//print(st0 ~= st1)

// MARK: - Comparable
/// Comparable confirms to `Equatable` and allow to Compare: <, >, <=, etc.

extension MyStruct: Comparable {
    static func < (lhs: MyStruct, rhs: MyStruct) -> Bool {
        return lhs.value < rhs.value
    }
}

// MARK: - Identifiable
extension MyStruct: Identifiable {
    var id: UUID {
        return UUID()
    }
}

var id0 = MyStruct(value: "0")
var id1 = MyStruct(value: "1")

id0.id == id1.id

// MARK: - Hashable
/// Many Basic Structures use `Hash` to define certain element
extension MyStruct: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
        hasher.combine(id)
    }
}

let hashed0 = MyStruct(value: "0")
let hashed1 = MyStruct(value: "0")
hashed0.hashValue == hashed1.hashValue

let set = Set<MyStruct>(arrayLiteral: hashed0, hashed1)
set.count

let hashedArray = [hashed0, hashed1]
hashedArray.count

// MARK: - CustomStringConvertible
extension MyStruct: CustomStringConvertible {
    var description: String {
        return "\(id) - \(value)"
    }
}


// MARK: - CustomDebugStringConvertible
extension MyStruct: CustomDebugStringConvertible {
    var debugDescription: String {
        return "DEBUG: \(value)"
    }
}

//print(hashed0)

//print(String(reflecting: hashed0))
//debugPrint(hashed0)


// MARK: - Enum
enum MyEnum: String, CaseIterable  {
    case north, south, east, west
}

let myEnum: MyEnum = .north
for ccc in MyEnum.allCases {
//    print(ccc)
    ()
//    north
//    south
//    east
//    west
}

//print(myEnum) ///  It is string, because of `RawRepresentable`
//print(myEnum.rawValue)

// MARK: - Option Sets
/// Make TwoWay Bidnings between different `Struct`
struct Directions: OptionSet {
    let rawValue: UInt8

    static let up    = Directions(rawValue: 1 << 0)
    static let down  = Directions(rawValue: 1 << 1)
    static let left  = Directions(rawValue: 1 << 2)
    static let right = Directions(rawValue: 1 << 3)
}

let allowedMoves: Directions = [.up, .down, .left]
print(allowedMoves.rawValue)

