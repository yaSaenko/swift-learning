import Foundation

struct Article: CustomStringConvertible {
    let title: String
    let body: String
    var author: String?

    var description: String {
        var result = "\(title):\n\t\(body)"
        if let author = author {
            result.append("\n\t\(author)")
        }
        return result
    }

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case body = "body"
        case author = "author"
    }

    
    enum AuthorKeys: String, CodingKey {
        case name = "name"
        case fullName = "fullName"
        case lastName = "lastName"
    }
}

extension Article: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        body = try values.decode(String.self, forKey: .body)
        
        if values.contains(.author) {
            let authorContainer = try values.nestedContainer(keyedBy: AuthorKeys.self, forKey: .author)
            if authorContainer.contains(.fullName) {
                let name = try authorContainer.decodeIfPresent(String.self, forKey: .fullName)
                author = "fullName: \(name ?? "")"
            }
            if authorContainer.contains(.lastName) {
                let name = try authorContainer.decodeIfPresent(String.self, forKey: .lastName)
                author = "lastName: \(name ?? "")"
            }
            if authorContainer.contains(.name) {
                let name = try authorContainer.decodeIfPresent(String.self, forKey: .name)
                author = "name: \(name ?? "")"
            }
        } else {
            self.author = nil
        }
    }
}

extension Article: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(body, forKey: .body)
        try container.encode(author, forKey: .author)
    }
    
}


let filePath = Bundle.main.path(forResource: "Articles", ofType: "json")!
let url = URL(fileURLWithPath: filePath)
let data = try! Data(contentsOf: url)
let decoder = JSONDecoder()

let articles = try! decoder.decode([Article].self, from: data)

articles.forEach { print($0) }


