import Foundation

struct MyStruct {
    var value: String
}

var object: MyStruct

// MARK: - ExpressibleByIntegerLiteral
extension MyStruct: ExpressibleByIntegerLiteral {
    init(integerLiteral value: IntegerLiteralType) {
        self.init(value: "ExpressibleByIntegerLiteral: \(value)")
    }
}
object = 10
print(object)

// MARK: - ExpressibleByFloatLiteral
extension MyStruct: ExpressibleByFloatLiteral {
    init(floatLiteral value: FloatLiteralType) {
        self.init(value: "ExpressibleByFloatLiteral: \(value)")
    }
}

object = 10.0
print(object)

// MARK: - ExpressibleByBooleanLiteral
extension MyStruct: ExpressibleByBooleanLiteral {
    init(booleanLiteral value: BooleanLiteralType) {
        self.init(value: "ExpressibleByBooleanLiteral: \(value)")
    }
}

object = true
print(object)

// MARK: - ExpressibleByNilLiteral
extension MyStruct: ExpressibleByNilLiteral {
    init(nilLiteral: ()) {
        self.init(value: "ExpressibleByNilLiteral: always nil")
    }
}

object = nil
print(object)

// MARK: - ExpressibleByStringLiteral
extension MyStruct: ExpressibleByStringLiteral {
    init(stringLiteral value: StringLiteralType) {
        self.init(value: "ExpressibleByStringLiteral: \(value)")
    }
}
object = "Hello!"
print(object)

// MARK: - ExpressibleByArrayLiteral
extension MyStruct: ExpressibleByArrayLiteral {
    typealias ArrayLiteralElement = String

    init(arrayLiteral elements: String...) {
        self.init(value: "ExpressibleByArrayLiteral: \(elements)")
    }
}

object = ["Hello", "It's", "Me"]
print(object)

// MARK: - ExpressibleByDictionaryLiteral
extension MyStruct: ExpressibleByDictionaryLiteral {
    typealias Key = String
    typealias Value = String

    init(dictionaryLiteral elements: (String, String)...) {
        self.init(value: "ExpressibleByDictionaryLiteral: \(elements)")
    }
}

object = ["First": "Hello",
          "Second": "It's",
          "Third": "Me"]
print(object)
