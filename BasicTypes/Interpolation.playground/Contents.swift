import Foundation

struct Student {
    let name: String
    let surname: String
    let age: Int
    let major: Major
    
    enum Major: String {
        case engineer = "Engineer"
        case programmer = "Programmer"
    }
    
    init(name: String, surname: String, age: Int, major: Major) {
        self.name = name
        self.surname = surname
        self.age = age
        self.major = major
    }
}

extension Student: CustomStringConvertible {
    var description: String {
        return "Name: \(name), Surname: \(surname)"
    }
}

extension String.StringInterpolation {
    mutating func appendInterpolation(student value: Student, isAge: Bool) {
        if isAge {
            appendLiteral("Name: \(value.name), Surname: \(value.surname), Age: \(value.age)")
        } else {
            appendLiteral("Name: \(value.name), Surname: \(value.surname)")
        }
    }
}

let student = Student(name: "Iaroslav", surname: "Saenko", age: 21, major: .programmer)

print("With String.StringInterpolation AGE=true \(student: student, isAge: true)")
print("With String.StringInterpolation AGE=false \(student: student, isAge: false)")
print("With description \(student)")


