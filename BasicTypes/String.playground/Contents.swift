import Foundation
/// `String` is a `Collection` of `Character`
/// `Character` is a Presenation of one Unicode symbol: ASCII symbol and other

let _: Character  = .init("A")

let airplane: Character = .init(Unicode.Scalar(9992)!)


let A: Character = "A"
let B: Character = "B"

A == B
A != B
A < B
A <= B
A >= B
A > B

let usFlag: Character = "\u{1F1FA}\u{1F1F8}"
usFlag.unicodeScalars
usFlag.isASCII
usFlag.asciiValue

usFlag.isLetter
usFlag.isPunctuation
usFlag.isNewline
usFlag.isWhitespace
usFlag.isSymbol
usFlag.isMathSymbol
usFlag.isCurrencySymbol

A.isCased
A.isUppercase
A.isLowercase
A.uppercased()
A.lowercased()

A.isWholeNumber
A.wholeNumberValue
A.isHexDigit
A.hexDigitValue

let _ = "A"..<"Z"

A.utf8
A.utf16

// MARK: - String

let _: String = .init("A")
let _: String = .init(repeating: "A", count: 5)
let _: String = .init(repeating: "AZ", count: 5)

//let _: String? = String(data: AB.data(using: .utf8)!, encoding: .utf8)

let AB: String = .init("AB")

AB.isEmpty
AB.count

let _: String = .init(format: "%d + %d", 1, 2)
let _: String = .init(format: "%d;  %d", locale: .init(identifier: "ru_RU"), 12, 10)

let _ = String(Int.max, radix: 16, uppercase: true)


let strA = "str"
let strB = "str"
print(strA == strB)
print(strA.elementsEqual(strB))

String.localizedName(of: .utf16)

var cString = strA.utf8CString
cString.append(CChar(clamping: 12))

("AA"..."ZZ").contains("ZAZ")
