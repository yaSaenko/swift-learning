import Foundation

infix operator ~

struct CustomInt {
    let value: Int

    static prefix func - (value: CustomInt) -> CustomInt {
        return CustomInt(value: -value.value)
    }

    static func + (left: CustomInt, right: CustomInt) -> CustomInt {
        return CustomInt(value: left.value + right.value)
    }

    static func +=(left: inout CustomInt, right: CustomInt) {
        left = left + right
    }
    
    static func == (left: CustomInt, right: CustomInt) -> Bool {
        return left.value == right.value
    }
    
    static func ^ (left: CustomInt, right: CustomInt) -> CustomInt {
        let powed = pow(Double(left.value), Double(right.value))
        let powedInt = Int(powed)
        return CustomInt(value: powedInt)
    }

    static func ~(value: CustomInt, isValue: Bool) -> CustomInt {
        let result = isValue ? Int.max : Int.min
        return .init(value: result)
    }
    
    static func ~(left: CustomInt, right: CustomInt) -> CustomInt {
        return .init(value: left.value * right.value)
    }
}

var custom0 = CustomInt(value: 12)
let custom1 = CustomInt(value: 20)

print(-custom0)
print(custom0 + custom1)
custom0 += -custom1
print(custom0 ^ custom1)
print(custom0~false)
print(custom0~custom0)

let neg = -12
let myNeg = UInt(bitPattern: neg)
