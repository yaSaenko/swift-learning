//
//  TransitionerVC.swift
//  ApplicationLifeCycle
//
//  Created by Iaroslav Saenko on 30.07.2020.
//  Copyright © 2020 Yaroslav Saenko. All rights reserved.
//

import UIKit
class TransitionerVC: UIViewController {
    
    /// Transition Delegate
    private let transition = FirstTransition()
    
    // MARK: - Interface
    let button: UIButton = {
        let button = UIButton(frame: .init(x: 150, y: 150, width: 100, height: 100))
        button.setTitle("Show VC", for: .normal)
        
        return button
    }()
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .random
        
        button.addTarget(self, action: #selector(showTrans), for: .touchUpInside)
        view.addSubview(button)
    }
    
    @objc func showTrans() {
        let vc = ViewController()
        vc.view.backgroundColor = .random

        vc.transitioningDelegate = transition
        vc.modalPresentationStyle = .custom

        present(vc, animated: false)
    }
}

class FirstTransition: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presenterContr = PresentationController(presentedViewController: presented, presenting: presenting ?? source)

        return presenterContr
    }
}

class PresentationController: UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        let bounds = containerView!.bounds
        let halfHeight = bounds.height / 2
        return CGRect(x: 0,
                             y: halfHeight,
                             width: bounds.width,
                             height: halfHeight)
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        containerView?.addSubview(presentedView!)
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
}
