//
//  ViewController.swift
//  ApplicationLifeCycle
//
//  Created by Iaroslav Saenko on 29.07.2020.
//  Copyright © 2020 Yaroslav Saenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let button = UIButton(frame: .init(x: view.center.x, y: 100, width: 100, height: 100))
        button.setTitle("Prees", for: .normal)
        button.addTarget(nil, action: #selector(showNext), for: .touchUpInside)
        view.addSubview(button)

        let deviceButton = UIButton(frame: .init(x: view.center.x, y: 300, width: 100, height: 100))
        deviceButton.setTitle("Device", for: .normal)
        deviceButton.addTarget(nil, action: #selector(showDeviceVC), for: .touchUpInside)
        view.addSubview(deviceButton)
        
        let vcButton = UIButton(frame: .init(x: view.center.x, y: 500, width: 100, height: 100))
        vcButton.setTitle("VC", for: .normal)
        vcButton.addTarget(nil, action: #selector(showTrans), for: .touchUpInside)
        view.addSubview(vcButton)
        
    }

    @objc func showNext() {
        let vc = ViewController()
        let color = UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1)
        vc.view.backgroundColor = color
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func ignoreEvents() {
        UIApplication.shared.beginIgnoringInteractionEvents()
    }

    @objc func remoteForRemoteNotifications() {
        UIApplication.shared.registerForRemoteNotifications()
        UIApplication.shared.unregisterForRemoteNotifications()
        if UIApplication.shared.isRegisteredForRemoteNotifications {
            ()
        }
    }

    @objc func applicationSupportsShakeToEdit() {
        UIApplication.shared.applicationSupportsShakeToEdit = true
    }

    @objc func canOpenURL() {
        if let url = URL(string: "") {
            if UIApplication.shared.canOpenURL(url) {
                print("Can open")
//                UIApplication.shared.open(url, options: []) { _ in }
            }
        }
    }

    var backgroundId: UIBackgroundTaskIdentifier!
    @objc func beginBackgroundTask() {
        backgroundId =  UIApplication.shared.beginBackgroundTask {
            print("beginBackgroundTask")
        }
        UIApplication.shared.endBackgroundTask(backgroundId)
        
        /// Show Debugger name
        backgroundId = UIApplication.shared.beginBackgroundTask(withName: "MyTask", expirationHandler: {
            print("beginBackgroundTask(withName:)")
        })
        UIApplication.shared.endBackgroundTask(backgroundId)
    }

    @objc func beginReceivingRemoteControlEvents() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        UIApplication.shared.endReceivingRemoteControlEvents()
    }

    @objc func applicationState() {
        let state = UIApplication.shared.applicationState
        switch state {
        case .active:
            print("active")
        case .inactive:
            print("inactive")
        case .background:
            print("background")
        @unknown default:
            print("default")
        }
    }

    @objc func userInterfaceLayoutDirection() {
        let direction = UIApplication.shared.userInterfaceLayoutDirection
        switch direction {
        case .leftToRight:
            print("leftToRight")
        case .rightToLeft:
            print("rightToLeft")
        @unknown default:
            print("default")
        }
    }

    @objc func showDeviceVC() {
        let vc = DeviceVC()
        let color = UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1)
        vc.view.backgroundColor = color
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func showTrans() {
        let vc = TransitionerVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}
