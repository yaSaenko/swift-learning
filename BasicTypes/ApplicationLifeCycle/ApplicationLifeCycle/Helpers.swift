//
//  File.swift
//  ApplicationLifeCycle
//
//  Created by Iaroslav Saenko on 30.07.2020.
//  Copyright © 2020 Yaroslav Saenko. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static let random = UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1)
}
