//
//  AppDelegate.swift
//  ApplicationLifeCycle
//
//  Created by Iaroslav Saenko on 29.07.2020.
//  Copyright © 2020 Yaroslav Saenko. All rights reserved.
//

import UIKit


///    `Application Launch Sequence`
///
///    main()
///    UIApplicationMain()
///    Load the main UI file  (if it is exists: it is Storyboard)
///    First initialization     --->    (_:willFinishLaunchingWithOptions:)
///    Restore UI state      --->    Some methods
///    Final Initialization    --->     (_:didFinishLaunchingWithOptions:)


///    `Application Life Cycle`
///
///    `After init becomes Foreground`
///    1. (_:willFinishLaunchingWithOptions:)
///    2. (_:didFinishLaunchingWithOptions:)
///    3. applicationDidBecomeActive(_:)
///
///    `Go to background` and `System notifications like AirPods and Low battery3`
///    1. applicationWillResignActive(_:)
///    2. applicationDidEnterBackground(_:)
///
///    `Go foreground from background`
///    1. applicationWillEnterForeground(_:)
///    2. applicationDidBecomeActive(_:)
///
///    `Terminate from Foreground`
///    1. applicationWillResignActive(_:)
///    2. applicationDidEnterBackground(_:)
///    3. applicationWillTerminate(_:)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: After init
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("After init")
        print("Calls 0: \(#function)")
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("Calls 1: \(#function)")
        
        let vc = ViewController()
        vc.view.backgroundColor = .orange
        let nc = UINavigationController(rootViewController: vc)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
        
        if let keys = launchOptions?.keys {
            if keys.contains(.location) {
                print("Location key")
            }
        }
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("Calls 2: \(#function)")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("Calls 3: \(#function)")
    }
    
    // MARK: Background
    func applicationWillResignActive(_ application: UIApplication) {
        print("Calls: \(#function)")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("Calls: \(#function)")
    }
    
    // MARK: Termination
    func applicationWillTerminate(_ application: UIApplication) {
        print("Calls: \(#function)")
    }
}

// MARK: - Restore App state
extension AppDelegate {
    func application(_ application: UIApplication, shouldSaveSecureApplicationState coder: NSCoder) -> Bool {
        return false
    }
    func application(_ application: UIApplication, shouldRestoreSecureApplicationState coder: NSCoder) -> Bool {
        return false
    }
    
    func application(_ application: UIApplication, didDecodeRestorableStateWith coder: NSCoder) {
    }
}

// MARK: SupportedInterfaceOrientation
extension AppDelegate {
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .all
    }
}
