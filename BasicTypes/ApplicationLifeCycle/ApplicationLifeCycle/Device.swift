//
//  Device.swift
//  ApplicationLifeCycle
//
//  Created by Iaroslav Saenko on 30.07.2020.
//  Copyright © 2020 Yaroslav Saenko. All rights reserved.
//

import UIKit

class DeviceVC: UIViewController {
    let device = UIDevice.current

    override func viewDidLoad() {
        super.viewDidLoad()
        print(device.name)
        print(device.systemName)
        print(device.systemVersion)
        print(device.model)
        print(device.localizedModel)
        print("Phone, pad, tv, carplay: ", device.userInterfaceIdiom)
        print(device.identifierForVendor ?? "")
        
        print(device.orientation)
        
        print(device.batteryLevel)
        print(device.proximityState)
        

        
        
        playSoundAfterSeconds {
            self.playSoundAfterSeconds()
        }
    }

    private func playSoundAfterSeconds(completion: (() -> Void)? = nil) {
        DispatchQueue.global().asyncAfter(deadline: .now() + .seconds(2)) {
            for _ in 1...10 {
                self.device.playInputClick()
            }
            completion?()
        }
    }
}
