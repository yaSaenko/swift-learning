import Foundation

struct Article {
    let title: String
    let body: String
}

let articles = ["A", "B", "C", "D"].map { Article(title: $0, body: $0) }

var articleTitles = articles.map { $0.title }
var articleBodies = articles.map { $0.body }

articleTitles = articles.map(\.title)
articleBodies = articles.map(\.body)

extension Sequence {
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in
            return a[keyPath: keyPath] < b[keyPath: keyPath]
        }
    }
}

let sorted = articles.sorted(by: \.body)

