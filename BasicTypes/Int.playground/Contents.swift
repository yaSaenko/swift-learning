import Foundation

let int = Int()
let _ = Int(bigEndian: 10)
let _ = Int(littleEndian: 10)
//let _ = Int(bitPattern: )
let _ = Int(bitPattern: 10000)
let _ = Int8(clamping: 1000)
let _ = Int("1000")
let _ = Int(exactly: 19999.1)
let _ = 23 // Int(integerLiteral: 23)

let _ = Int.random(in: 1...100)

// Return (quotient, remainder) = (2, 5)
10.quotientAndRemainder(dividingBy: 25)

20.isMultiple(of: 10)

// Return (value, isOverFlower)
20.addingReportingOverflow(Int.max)
20.subtractingReportingOverflow(Int.max)
20.multipliedReportingOverflow(by: Int.max)
20.dividedReportingOverflow(by: Int.max)
20.remainderReportingOverflow(dividingBy: Int.max)

let x: UInt8 = 100
let y: UInt8 = 20
_ = x.multipliedFullWidth(by: y)
_ = x.dividingFullWidth((high: UInt8.max, low: UInt8.min))

-150.magnitude // same as ABS

-150.signum()

_ = Int.zero
_ = Int.min
_ = Int.max
_ = Int.isSigned

100.description

// MARK: - Double


