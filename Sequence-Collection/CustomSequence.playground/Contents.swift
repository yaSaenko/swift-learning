printList("Common init") {
    return LinkedList<Int>(1)
}

printList("Array") { () -> LinkedList<Int> in
    let list: LinkedList<Int> = [1, 2, 3]
    return list
}

