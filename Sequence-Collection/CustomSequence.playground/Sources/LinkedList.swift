import Foundation

/// Linked list structure
public struct LinkedList<T> {
    public var head: Node<T>
    
    public init(_ value: T) {
        head = Node.value(element: value, next: .end)
    }

    public var isEmpty: Bool {
        switch head {
        case .end:
            return true
        case .value:
            return false
        }
    }
}

// MARK: - Sequence
/// Confirm to Sequence protocol by `makeIterator()` with `LinkedListIterator<T>` as an `Iterator`
extension LinkedList: Sequence {
    public func makeIterator() -> LinkedListIterator<T> {
        return LinkedListIterator<T>(current: head)
    }
}

// MARK: - CustomStringConvertible
/// Add `Description` property for calling in `print()`
extension LinkedList: CustomStringConvertible {
    public var description: String {
        let splitter = ", "
        var string = self.reduce(into: "") { (resultString, element) in
                resultString.append("\(element)\(splitter)")
        }
        string.removeLast(splitter.count)
        return string
    }
}


// MARK: - Cusom Init



// MARK: ExpressibleByArrayLiteral
/// Allow to `Init` instance as array of `T`
///
/// `let LinkedList =  [T, T, T, T]`
extension LinkedList: ExpressibleByArrayLiteral {
    public init(arrayLiteral elements: T...) {
        var current = Node<T>.end
        for element in elements.reversed() {
            current = Node.value(element: element, next: current)
        }
        self.head = current
    }
}

