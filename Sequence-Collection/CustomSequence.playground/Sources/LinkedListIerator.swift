import Foundation

public struct LinkedListIterator<T>: IteratorProtocol {
    public var current: Node<T>

    public typealias Element = T
    public mutating func next() -> T? {
        switch current {
        case .end:
            return nil
        case let .value(element, nextNode):
            current = nextNode
            return element
        }
    }
}
