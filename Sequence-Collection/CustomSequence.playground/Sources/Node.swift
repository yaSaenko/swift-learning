import Foundation

public indirect enum Node<T> {
    case value(element: T, next: Node<T>)
    case end
}
