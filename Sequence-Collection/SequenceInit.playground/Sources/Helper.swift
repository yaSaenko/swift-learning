import Foundation

public func printSequenceInit<T: Sequence>(_ typeOfInit: String, completion: () -> T) {
    print("Type of init:", typeOfInit)
    let list = completion()
    print("List:",list)
    print()
}

