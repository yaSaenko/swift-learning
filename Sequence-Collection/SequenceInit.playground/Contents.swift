import UIKit

let array = [1,2,3,4]

printSequenceInit("Instance") { () -> Array<Int> in
    var array = Array<Int>()
    array.append(1)
    return array
}

printSequenceInit("In array") { () -> Array<Int> in
    return [1,2,3]
}

printSequenceInit("ExpressibleByArrayLiteral") { () -> Array<Int> in
    let array = Array<Int>(arrayLiteral: 1, 2)
    return array
}

printSequenceInit("Repeated") { () -> Array<Int> in
    let array = Array<Int>(repeating: 1, count: 5)
    return array
}

/// If `Completion` return `nil` it stops
printSequenceInit("Sequence(first, (_)-> T? )") { () -> Array<Int> in
    let array = Array<Int>(sequence(first: 1, next: { (element) -> Int? in
        return Int.random(in: 1...2) % 2 == 0 ? element + 1 : nil
    }))
    return array
}

/// `ClosedRange` is a `Sequence`
printSequenceInit("By Range") { () -> Array<Int> in
    let array = Array<Int>(0...10)
    return array
}

