import Foundation

/// Range 0..<3 - 0,1,2
/// ClosedRange 0...3 - 1,2,3
/// PartialRangeUpTo - ..<5
/// PartialRangeThrough - ...5
/// PartialRangeFrom - 5...

/// Conforms To: Collection, Codable, Hashable, Equatable, CustomReflectable, CustomString, BidirictionalCollection



/// Init take ClosedRange
let myRange = Range(0...10)

/// Need deeper
print(myRange.relative(to: [-3,-2,-1,0,1,2,3]))

// MARK: - Propertier
myRange.lowerBound
myRange.upperBound
myRange.count
myRange.isEmpty

// MARK: - Check containing
myRange.contains(2)
print(myRange.contains(where: { $0 < 2 }))
print(myRange.allSatisfy({ $0 < 10}))

/// Check is `Value` in `Range`
print(myRange ~= 0)

/// Get `Range` confirmed to `both` `Ranges`
print(myRange.clamped(to: -10..<4))

// MARK: - Access
myRange.first
myRange.last

print(myRange[1])
print(myRange[1...3])
print(myRange[5...])
print(myRange.randomElement()!)

// MARK: - Find

myRange.first
myRange.last
myRange.min()
myRange.max()

myRange.firstIndex(of: 10)
myRange.lastIndex(of: 3)

// MARK: - Describle
print(myRange.description)
print(myRange.debugDescription)
print(myRange.customMirror)


// MARK: - PartialRangeFrom
let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
let asciiTable = zip(65..., alphabet)
for (code, letter) in asciiTable {
    print(code, letter)
}

// MARK: - PartialRangeThrough
let numbers = [10, 20, 30, 40, 50, 60, 70]
print(numbers[...3])

// MARK: - PartialRangeUpTo
print(numbers[..<3])
