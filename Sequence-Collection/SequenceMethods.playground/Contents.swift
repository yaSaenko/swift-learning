import Foundation

// MARK: - Finding
let array = Array<Int>(1...10)
/// Find  element in `Sequence` returns `Bool`
print(array.contains(1))

/// `constains(where:)` Find element with predicate
print(array.contains { element -> Bool in
    return element % 2 == 0
})

print(array.first { $0 == 2}!)

print(array.min()!)

/// `Precidate` is a `Comparator`
print(array.min { $0 > $1 }!)

print(array.max()!)

/// `Precidate` is a `Comparator`
print(array.max { $0 < $1 }!)

// MARK: - Map, FlatMap, Reduce
print("\n// MARK: - Map, FlatMap, Reduce")
/// Get `Sequence` and return `Sequence` without `nil`
let doubleNilArray: [[Int]?] = [[1,2,3], nil, [3,2,1], [4,5,6], [6,5,4]]
print(doubleNilArray.compactMap{ $0 })

/// Get `Sequence` of `Sequence` and Flat it to `Sequence`
let doubleArray: [[Int]] = [[1,2,3], [3,2,1], [4,5,6], [6,5,4]]
print(doubleArray.flatMap { $0 })
let stringArray = ["Abc", "Bcd"]
print(stringArray.flatMap{ $0 })
let doubleStringArray = [stringArray, stringArray]
print(doubleStringArray.flatMap{ $0 })

/// Combine `Sequence` with the closure
let reduceArray = "StringStr"
let reduceResult0 = reduceArray.reduce(0) { (inoutResult, char) -> Int in
    let integer = Int(char.asciiValue ?? 0)
    return inoutResult + integer
}
print(reduceResult0)

/// Transfomring `Sequence` to `into form`
let reduceResult1 = reduceArray.reduce(into: [:]) { (dictionary, char) in
    return dictionary[char, default: 0] += 1
}
print(reduceResult1)

// MARK: - Lazy sequence
print("\n// MARK: - Lazy sequence")
/// Lazy sequence: it is always like old `Sequence`, but take certain element should calculated. Methods: `Map`, `Filter`
let numbers = Array(1...10)
let doubled = numbers.map { $0 * 2 }
let lazyDoubled = numbers.lazy.map { $0 * 2 }

print(doubled)
print(lazyDoubled)
print(lazyDoubled.last!)
print(lazyDoubled)

// MARK: - Enumerated
print("\n// MARK: - Enumerated")
let enumeratedArray = ["A", "B", "C", "D"]
print(enumeratedArray)
for (key, char) in enumeratedArray.enumerated() {
    print(key, char)
}

// MARK: - Sort
print("\n// MARK: - Sort")
var sortedInitArray: [Int] = []
for _ in 0..<10 {
    sortedInitArray.append(Int.random(in: 0...10))
}
print(sortedInitArray)

let sortedArray0 = sortedInitArray.sorted()
print(sortedArray0)

let sortedArray1 = sortedInitArray.sorted { first, second -> Bool in
    return first < second
}
print(sortedArray1)

/// Such way `reversedArray` would be `ReversedCollection<[Int]>`
let reversedArray0 = sortedInitArray.reversed()
print(reversedArray0)

/// Such way `reversedArray` would be `[Int]`
let reversedArray1: [Int] = sortedInitArray.reversed()
print(reversedArray1)


// MARK: - Split
/// Available when Element conforms to Sequence.
print("\n// MARK: - Split")

/// Split `Sequence` to  `Sequence` of `Sequence`, at least Sequence.count+1
let unsplitedStr = "A B C D"
/// Available when Element conforms to StringProtocol.
let splitedStrFinal = unsplitedStr.split(separator: " ")
print(unsplitedStr)
print(splitedStrFinal)

let unsplitedInt = [1,2,3,4,5,2,6]

/// Get `Sequence` and try to Split it to smaller `Sequence` starting from the begining.
let splitedInt = unsplitedInt.split(separator: 2)
/// The same as above, becase maxSplits == Int.max, omittingEmptySubsequences = true by default
let splitedInt0 = unsplitedInt.split(separator: 2, maxSplits: 3, omittingEmptySubsequences: false)


/// Get `Sequence` and try to Split it to smaller `Sequence` starting from the begining.
/// Find element `doesn't confirmed` to `Separator` and Split this `Sequence` to 2.
/// If `right` is `Empty` `omittingEmptySubsequences` is `false`right would be taken to the `Resulted Sequence`
/// maxSplits = Amount of splits
let splitedInt1 = unsplitedInt.split(maxSplits: 3, omittingEmptySubsequences: false) { $0 % 2 == 0 }
/// The same as above, becase maxSplits == Int.max, omittingEmptySubsequences = true by default
let splitedInt2 = unsplitedInt.split { $0 % 2 == 0 }


print("unsplitedInt\n", unsplitedInt)
print("splitedInt\n", splitedInt)
print("split(separator: , maxSplits: , omittingEmptySubsequences: ) -> [ArraySlice<Int>]", splitedInt0)
print("split(maxSplits: , omittingEmptySubsequences: , whereSeparator: -> Bool) -> [ArraySlice<>]\n", splitedInt1)

// MARK: - Join
/// Available when Element conforms to Sequence.
print("\n// MARK: - Join")

let unjoinedArray = ["A", "B", "C", "D"]
let joinedArray0 = unjoinedArray.joined()
print(joinedArray0)

let joinedArray1 = unjoinedArray.joined(separator: ", ")
print(joinedArray1)

let unjoinerArrayOfArray = [[1,2,3], [4,5,6]]
let joinedArray2 = unjoinerArrayOfArray.joined(separator: [-1, -1])
print(Array(joinedArray2))

// MARK: - Comparing
/// Available when Element conforms to Equatable.
print("\n // MARK: - Comparing")
let comp0 = [1,2,3,4]
let comp1 = [3,4,5,6]

/// Get element one by one from both sequences
comp0.elementsEqual(comp1, by: { first, second -> Bool in
    return first > second
})
comp0.elementsEqual(comp1)

comp0.starts(with: [1,2])

/// Comparing by lexicographica like in Dictionary | O(n)
comp0.lexicographicallyPrecedes([1,2])

comp0.underestimatedCount

// MARK: - Instance Methods
print("\n// MARK: - Instance Methods")
var satis = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

/// Checking `Sequence` is all members `Satisfy` to `Predicate`
print(satis.allSatisfy { type(of: $0) == Int.self})

print(satis.dropFirst())
print(satis.dropFirst(2))
print(satis.dropLast())
print(satis.dropLast(2))
print(satis.drop { $0 < 10 })
print(satis.filter { $0 % 3 == 0})
satis.forEach { $0 % 5 == 0 ? print($0) : () }

// MARK: - Prefix, Suffix
print("\n// MARK: - Prefix, Suffix")
print(satis.prefix(3))
print(satis.prefix(upTo: 7))
print(satis.prefix(while: { $0 < 5 }))
/// Take prefix by index value (through+1)
print(satis.prefix(through: 3))

print(satis.suffix(3))
print(satis.suffix(from: 7))

// MARK: - Shuffled
print("\n// MARK: - Shuffled")
print(satis.shuffled())

