import Foundation

public func printSequence<T: Sequence>(_ typeOf: String, completion: () -> T) {
    print("Type of method:", typeOf)
    let list = completion()
    print("List:",list)
    print()
}

