import Foundation

/// Set is Collection, Collection is a Sequence
/// struct Set<Element> where Element : Hashable
/// Set has Capacity: size without need to new allocate

let commonSet: Set<Int> = Set()

let arrayLiteralSet = Set<Int>(arrayLiteral: 1,2,3,4)

let minimumCapacitySet = Set<Int>(minimumCapacity: 5)

var sequenceSet = Set<Int>(0...5)

// MARK: - Adding

sequenceSet.insert(100)
sequenceSet.contains(100)

// Update element
sequenceSet.update(with: 100)

// Reserve certain capacity of set
sequenceSet.reserveCapacity(100)

// MARK: - Remove

//print(sequenceSet.filter { $0 < 5 })
sequenceSet.remove(2)
sequenceSet.removeFirst()
sequenceSet.removeAll(keepingCapacity: true)
sequenceSet.removeAll()

// MARK: - Comparing union
let set0 = Set<Int>(arrayLiteral: 1,2,3,4,5)
let set1 = Set<Int>(arrayLiteral: 9,8,7,6)

set0[set0.startIndex]

// Returs union of sets
let unionedSet = set0.union(set1)

// Insert set to set
var formUnion = set0
formUnion.formUnion(set1)

// Return Intersection (пересечение)
let intersectionSet = set0.intersection(set1)

// Delete elements, that are not in Set
var formIntersectionSet = set0
formIntersectionSet.formIntersection(set1)

// Returns a new set with the elements that are either in this set or in the given sequence, but not in both.
let symetric = set0.symmetricDifference(set1)

var symetricSet = set0
symetricSet.formIntersection(set1)

// Removes the elements of the given set from this set. Work the same with Sequence
let substracted = set0.subtracting(set1)

var substractedSet = set0
substractedSet.subtract(set1)


// MARK: - Operators

set0 == set1 ? () : ()
set0 != set1 ? () : ()

set0.isSubset(of: set1) ? () : ()
set0.isStrictSubset(of: set1) ? () : ()
set0.isSuperset(of: set1) ? () : ()
set0.isStrictSuperset(of: set1) ? () : ()

// No members in common with the given set.
set0.isDisjoint(with: set1) ? () : ()

