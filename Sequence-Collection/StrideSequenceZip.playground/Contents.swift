/// Store and organize data using arrays, dictionaries, sets, and other data structures.

// MARK: - Stride

/// Returns `Sequence` `from` `to` with by `stepping`
let stridedTo = stride(from: 0, to: 10, by: 3)
print(Array(stridedTo))

/// Returns `Sequence` `from` `to` with by `stepping`
let stridedThrough = stride(from: 0, through: 10, by: 3)
print(Array(stridedThrough))

// MARK: - Sequence
let sequenceFirst = sequence(first: 0) { $0 + 1 < 5 ? $0 + 1 : nil
}
sequenceFirst.map { print($0) }

/// Need to go deeper
//let sequenceState = sequence(state: currentState) { state -> T? in
//    state = Int.random(in: 0..1) % 2 == 0 ? true : false
//    if state {
//        return 1
//    } else {
//    return 0
//}

// MARK: - Zip
/// Creates a sequence of pairs built out of two underlying sequences. If a.count != b.count pass through them
let zipped = zip([1,2,3], [3,2,1, 0, 1])
print(Array(zipped))
