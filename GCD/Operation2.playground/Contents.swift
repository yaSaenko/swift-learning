/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import Foundation
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

// MARK: - START

let operationQueue = OperationQueue()

class OperationCancelTest: Operation {
    override func main() {
        if isCancelled {
            printThread("\(isCancelled)")
            return
        }
        printThread("test1")
        sleep(1)
        
        if isCancelled {
            printThread("\(isCancelled)")
            return
        }
        printThread("test 2")
    }
}

func cancelMethod() {
    let operation = OperationCancelTest()
    operationQueue.addOperation(operation)
    operation.cancel()
}

//cancelMethod()

// MARK: -
class WaitOprationTest {
    private let queue = OperationQueue()
    
    func test() {
        queue.addOperation {
            sleep(1)
            printThread("test 1")
        }
        queue.addOperation {
            sleep(1)
            printThread("test 2")
        }
        queue.waitUntilAllOperationsAreFinished()
        queue.addOperation {
            printThread("test 3")
        }
        queue.addOperation {
            printThread("test 4")
        }
    }
}

//let waitOp = WaitOprationTest()
//waitOp.test()

// MARK: -
class WaitOprationTest2 {
    private let queue = OperationQueue()
    
    func test() {
        let operation1 = BlockOperation {
            sleep(1)
            printThread("test 1")
        }

        let operation2 = BlockOperation {
            sleep(1)
            printThread("test 2")
        }
        
        queue.addOperations([operation1, operation2], waitUntilFinished: true)
        
        queue.addOperation {
            printThread("All finished")
        }
    }
}

//let waitOp = WaitOprationTest2()
//waitOp.test()

// MARK: -
class CopletionBlockTest {
    private let queue = OperationQueue()
    
    func test() {
        let operation1 = BlockOperation {
            printThread("CopletionBlockTest")
        }
        operation1.completionBlock = {
            printThread("Finish")
        }

        queue.addOperation(operation1)
    }
    
}

let completionOp = CopletionBlockTest()
completionOp.test()

