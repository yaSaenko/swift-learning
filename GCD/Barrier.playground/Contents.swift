/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import Foundation
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

//var array = [Int]()
//DispatchQueue.concurrentPerform(iterations: 10) {  id in
//    array.append(id)
//}
//
//print(array.count)
//print(array)


class SafeArray<T> {
    private var array = [T]()
    private let queue = DispatchQueue(label: "Queue", attributes: .concurrent)

    public func append(_ value: T) {
        queue.async(flags: .barrier) {
            self.array.append(value)
        }
    }
    
    public var valueArray: [T] {
        var result = [T]()
        queue.sync(flags: .barrier) {
            result = self.array
        }
        return result
    }
}

var arraySafe = SafeArray<Int>()
DispatchQueue.concurrentPerform(iterations: 10) { id in
    arraySafe.append(id)
}

print(arraySafe.valueArray)
