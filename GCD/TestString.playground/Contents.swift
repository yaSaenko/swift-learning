import UIKit
import PlaygroundSupport

func printThread(text: String) {
    print("\(text)\n\t\(Thread.current)")
}

class MyVC: UIViewController {
    var button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "XV1"
        view.backgroundColor = .green
        button.addTarget(self, action: #selector(buttonPushed), for: .touchUpInside)
        
//        afterBlock(seconds: 2) {
//            print("Hello!")
//            print(Thread.current)
//            DispatchQueue.main.async {
//                self.showAlert()
//            }
//        }
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: nil, message: "Hello", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }

    func afterBlock(seconds: Int, queue: DispatchQueue = .global(), completion: @escaping () -> ()) {
        queue.asyncAfter(deadline: .now() + .seconds(seconds)) {
            completion()
        }
    }

    @objc private func buttonPushed() {
        let vc = NewVC()
        vc.title = "VC 2"
        vc.view.backgroundColor = .orange
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initButton()
    }
    
    private func initButton() {
        button.frame = .init(x: 0, y: 0, width: 200, height: 50)
        button.center = view.center
        button.setTitle("fdsf", for: .normal)
        button.backgroundColor = .red
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        
        view.addSubview(button)
    }
}

class NewVC: UIViewController {
    let image = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "VC2"
        view.backgroundColor = .orange

//        loadPhoto()
//        DispatchQueue.global().async {
//            DispatchQueue.concurrentPerform(iterations: 20000) { num in
//                print(num)
//                print(Thread.current)
//            }
//        }
        myInactiveQueue()
    }
    
    func myInactiveQueue() {
        let inactiveQueue = DispatchQueue(label: "queueue", attributes: [.concurrent, .initiallyInactive])

        inactiveQueue.async {
            printThread(text: "Done!")
        }

        printThread(text: "Not yet started")
        
        inactiveQueue.activate()
        printThread(text: "Activate")
        
        inactiveQueue.suspend()
        printThread(text: "Suspend")
        
        inactiveQueue.resume()
        printThread(text: "Resume")
        
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initButton()
    }

    private func initButton() {
        image.frame = .init(x: 0, y: 0, width: 350, height: 500)
        image.center = view.center

        view.addSubview(image)
    }
    
//    private func loadPhoto() {
//        let imageURL = URL(string: "http://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!
//        let queue = DispatchQueue.global(qos: .utility)
//        queue.async {
//            if let data = try? Data(contentsOf: imageURL) {
//                DispatchQueue.main.async {
//                    self.image.image = UIImage(data: data)
//                }
//            }
//        }
//    }
}

let vc = MyVC()
let navBar = UINavigationController(rootViewController: vc)

PlaygroundPage.current.liveView = navBar
