/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import UIKit
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true



class DispatchWorkItem1 {
    private let queue = DispatchQueue(label: "DispatchWorkItem1", attributes: .concurrent)
    
    func create() {
        let workItem = DispatchWorkItem {
            printThread("Start task")
        }

        workItem.notify(queue: .main) {
            printThread("Task finish")
        }
        
        queue.async(execute: workItem)
    }
}

class DispatchWorkItem2 {
    private let queue = DispatchQueue(label: "DispatchWorkItem2", attributes: .concurrent)
    
    func create() {
        queue.async {
            sleep(1)
            printThread("Task 1")
        }
        
        queue.async {
            sleep(1)
            printThread("Task 2")
        }
        
        let workItem = DispatchWorkItem {
            printThread()
            printThread("Start work item Task")
        }

        queue.async(execute: workItem)
        queue.async {
            workItem.cancel()
        }
        
    }
}


// MARK: - Prepare view and image
let imageUrl = URL(string: "http://planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!

var view = UIView(frame: .init(x: 0, y: 0, width: 500, height: 500))
var tower = UIImageView(frame: .init(x: 0, y: 0, width: 500, height: 500))
view.backgroundColor = .orange
view.contentMode = .scaleAspectFit
view.addSubview(tower)

PlaygroundPage.current.liveView = view

// MARK: - Classic
func fetchImage() {
    let queue = DispatchQueue.global(qos: .utility)
    
    queue.async {
        if let data = try? Data(contentsOf: imageUrl) {
            DispatchQueue.main.async {
                tower.image = UIImage(data: data)
            }
        }
    }
}
//fetchImage()

// MARK: - WorkItem
func fetchImageWorkItem() {
    var data: Data?
    
    let queue = DispatchQueue.global(qos: .utility)
    
    let workItem = DispatchWorkItem(qos: .userInteractive) {
        data = try? Data(contentsOf: imageUrl)
        printThread("\(#function): DispatchWorkItem)")
    }
    
    queue.async(execute: workItem)
    
    // Notify работает после выполнения самое WorkItem
    workItem.notify(queue: .main) {
        if let data = data {
            tower.image = UIImage(data: data)

            printThread("\(#function): workItem.notify(queue: .main)")
        }
    }
}
//fetchImageWorkItem()

// MARK: - URLSession
func fetchUrlSession() {
    let task = URLSession.shared.dataTask(with: imageUrl) { data, response, error in
        printThread("\(#function): load image")
        if let data = data {
            DispatchQueue.main.async {
                tower.image = UIImage(data: data)
                printThread("\(#function): set image")
            }
        }
    }
    task.resume()
}
fetchUrlSession()
