/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import Foundation
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

let timer = DispatchSource.makeTimerSource(queue: .global())
timer.setEventHandler {
    printThread("Hello!")
}

timer.schedule(deadline: .now(), repeating: 5)

timer.activate()
