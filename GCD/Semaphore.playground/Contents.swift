/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import UIKit
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

let queue = DispatchQueue(label: "Queueue", attributes: .concurrent)

// Amount of closures executes in
//let semaphore = DispatchSemaphore(value: 1)

//queue.async {
//    semaphore.wait()
//    sleep(3)
//    printThread("method 1")
//    semaphore.signal()
//}
//
//queue.async {
//    semaphore.wait()
//    sleep(3)
//    printThread("method 2")
//    semaphore.signal()
//}
//
//queue.async {
//    semaphore.wait()
//    sleep(3)
//    printThread("method 3")
//    semaphore.signal()
//}

//let sem = DispatchSemaphore(value: 10)

//DispatchQueue.concurrentPerform(iterations: 50) { id in
//    sem.wait(timeout: .distantFuture)
//    printThread("Blocked \(id)")
//    sleep(1)
//    sem.signal()
//}

class SemaphoreTest {
    private let semaphore = DispatchSemaphore(value: 4)
    
    var array = [Int]()
    
    private func work(_ id: Int) {
        semaphore.wait()
        array.append(id)
        printThread("Test array, \(array.count)")
        sleep(1)
        semaphore.signal()
    }
    
    func startAllThread() {
        DispatchQueue.global().async {
            self.work(Int.random(in: 1...100))
//            printThread(#function)
        }
        DispatchQueue.global().async {
            self.work(Int.random(in: 1...100))
//            printThread(#function)
        }
        DispatchQueue.global().async {
            self.work(Int.random(in: 1...100))
//            printThread(#function)
        }
        DispatchQueue.global().async {
            self.work(Int.random(in: 1...100))
//            printThread(#function)
        }
        DispatchQueue.global().async {
            self.work(Int.random(in: 1...100))
//            printThread(#function)
        }
    }
}

let semTest = SemaphoreTest()
semTest.startAllThread()
