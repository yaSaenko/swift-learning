/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import Foundation
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

//printThread()

//let operation1 = {
//    printThread("start")
//    printThread("end")
//}
//
//let queue = OperationQueue()
//queue.addOperation(operation1)

//var result: String?
//let concatOperation = BlockOperation {
//    result = "The swift nice"
//    printThread()
//}
//
//let queue = OperationQueue()
//queue.addOperation(concatOperation)
//printThread(result)
//
//let queue1 = OperationQueue()
//queue1.addOperation {
//    printThread("fdsf")
//}


// MARK: - Thread
class MyThread: Thread {
    override func main() {
        printThread("Test main thread")
    }
}

let myThread = MyThread()
//myThread.start()

// MARK: - Operation
class OperationA: Operation {
    override func main() {
        printThread("Test main operation")
    }
}

let operationA = OperationA()
//operationA.start()

let queue = OperationQueue()
queue.addOperation(operationA)
