/// Print text with info about Current Thread
func printThread(_ text: String? = nil) {
    guard let text = text else {
        print("\(Thread.current): \n\tnil")
        return
    }
    print("\(Thread.current): \n\t \(text)")
}

import UIKit
import PlaygroundSupport

// Wait for Threads cancel
PlaygroundPage.current.needsIndefiniteExecution = true

let imageURLs = ["https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg", "http://adriatic-lines.com/wp-content/uploads/2015/04/canal-of-Venice.jpg", "http://bestkora.com/IosDeveloper/wp-content/uploads/2016/12/Screen-Shot-2017-01-17-at-9.33.52-PM.png", "http://adriatic-lines.com/wp-content/uploads/2015/04/canal-of-Venice.jpg" ]


class DispatchGroupTest1 {
    private let queue = DispatchQueue(label: "queueueue")
//    private let queue = DispatchQueue(label: "queueueue", attributes: .concurrent)
    
    private let groupRed = DispatchGroup()
    
    func loadInfo() {
        queue.async(group: groupRed) {
            sleep(1)
            printThread("GroupRed 0 ")
        }
        
        queue.async(group: groupRed) {
            sleep(1)
            printThread("GroupRed 1")
        }
        
        groupRed.notify(queue: .main) {
            printThread("Finish all")
        }
    }
}

class DispatchGroupTest2 {
    private let queue = DispatchQueue(label: "queueueue")
//    private let queue = DispatchQueue(label: "queueueue", attributes: .concurrent)
    
    private let groupBlack = DispatchGroup()
    
    func loadInfo() {
        groupBlack.enter()
        queue.async {
            sleep(1)
            printThread("GroupBlack 0")
            self.groupBlack.leave()
        }
        
        groupBlack.enter()
        queue.async {
            sleep(1)
            printThread("GroupBlack 1")
            self.groupBlack.leave()
        }
        
        groupBlack.wait()
        
        groupBlack.notify(queue: .main) {
            printThread("Finish all blacks")
        }
        
        printThread("Prepare for finish")
    }
}

// MARK: - Start
let group = DispatchGroupTest2()
//group.loadInfo()


// MARK: - UIKit example

class EightImage: UIView {
    public var ivs = [UIImageView]()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        ivs.append(UIImageView(frame: .init(x: 0, y: 0, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 0, y: 100, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 100, y: 0, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 100, y: 100, width: 100, height: 100)))
        
        ivs.append(UIImageView(frame: .init(x: 0, y: 300, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 100, y: 300, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 0, y: 400, width: 100, height: 100)))
        ivs.append(UIImageView(frame: .init(x: 100, y: 400, width: 100, height: 100)))
        
        for i in 0...7 {
            ivs[i].contentMode = .scaleAspectFit
            self.addSubview(ivs[i])
        }
    }

    required init?(coder: NSCoder) {
        fatalError(#function)
    }
}

var view = EightImage(frame: .init(x: 0, y: 0, width: 700, height: 700))
view.backgroundColor = .orange

PlaygroundPage.current.liveView = view

func asyncLoadImage(imageUrl: URL,
                    runQueue: DispatchQueue,
                    completionQueue: DispatchQueue,
                    completion: @escaping (UIImage?, Error?) -> ()) {
    runQueue.async {
        do {
            let data = try Data(contentsOf: imageUrl)
            completionQueue.async {
                let image = UIImage(data: data)
                completion(image, nil)
            }
        } catch {
            completion(nil, error)
        }
    }
}

var images: [UIImage] = []

func asyncGroup() {
    let aGroup = DispatchGroup()
    
    for i in 0...3 {
        aGroup.enter()
        asyncLoadImage(imageUrl: URL(string: imageURLs[i])!,
                       runQueue: .global(),
                       completionQueue: .main) { image, error in
                        guard let image = image else { return }
                        images.append(image)
                        aGroup.leave()
                        
        }
    }
    
    aGroup.notify(queue: .main) {
        for i in 0...3 {
            view.ivs[i].image = images[i]
        }
    }
}

func asyncUrlSession() {
    for i in 4...7 {
        let url = URL(string: imageURLs[i - 4])!
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                view.ivs[i].image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}


asyncGroup()
asyncUrlSession() 
