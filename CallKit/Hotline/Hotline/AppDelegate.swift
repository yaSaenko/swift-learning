//
//  AppDelegate.swift
//  Hotline
//
//  Created by Iaroslav Saenko on 29.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var callManager = CallManager()
    var providerDelegate: ProviderDelegate!
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        providerDelegate = ProviderDelegate(callManager: callManager)
        return true
    }

    func displayIncomingCall(uuid: UUID,
                             handle: String,
                             hasVideo: Bool = false,
                             completion: ((Error?) -> Void)?) {
        providerDelegate.reportIncomingCall(uuid: uuid, handle: handle, hasVideo: hasVideo, completion: completion)
    }
}

